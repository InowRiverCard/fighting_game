import { fightersDetails, fighters } from './mockData';

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const WR_API_URL = 'https://street-fighter-api.herokuapp.com/';
const useMockAPI = true;

async function callApi(endpoint, method) {
  const url = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint) {
  const response = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint) {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === id);
}

function increaseWR(fighter, endpoint = 'increase_wr') {
  const { name } = fighter;
  const url = WR_API_URL + endpoint;
  const myInit = {
    method: 'POST',
    body: JSON.stringify(name)
  };
  fetch (url, myInit)
    .catch((error) => {
      console.warn(error);
    });;
}

async function getWinnerList (endpoint = 'get_fighters') {
  const url = WR_API_URL + endpoint;
  return fetch(url)
    .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
    .catch((error) => {
      throw error;
    });
}

export { callApi,  getWinnerList, increaseWR };
