import { callApi } from '../helpers/apiHelper';

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/fighters.json';

const responsePromise = fetch(API_URL);
console.log(responsePromise);
 export async function getFighters() {
   try {
     const endpoint = 'fighters.json';
     const apiResult = await callApi(endpoint, 'GET');
     
     return apiResult;
   } catch (error) {
     throw error;
   }
 }
 
 export async function getFighterDetails(id) {
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }  
}


