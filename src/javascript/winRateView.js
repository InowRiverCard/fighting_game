import { createElement } from './helpers/domHelper'; 
import { showWinnerModal } from './modals/winRate';
import { getWinnerList } from './helpers/apiHelper'

export function createButton() {
    const tableContainer = createElement({ tagName: 'div', className: 'win-button' });
    const button = createElement({tagName: 'button', className: 'btn' });
    button.innerHTML = "Show Win Rate";
    const winners = getWinnerList();
    button.addEventListener('click', function showModal() {
        showWinnerModal(winners);
    }, false);
    tableContainer.appendChild(button);
    return tableContainer;
}