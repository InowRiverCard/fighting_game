import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';


export async function showWinnerModal(winners) {
  const title = 'Win Rate';
  const bodyElement = await createBody(winners);
  showModal({ title, bodyElement });
}
  
async function createBody(winners) {
  const fightersList = createElement({ tagName: 'div', className: 'modal-body' });
  let fighters;
  try {
    fighters = await winners;
  } catch (error) {
    console.warn(error);
    fightersList.innerText = 'Failed to load data';
  }
  
  if (fighters != null) {
    fighters.forEach(fighter => {
        let recordElement = createElement({ tagName: 'h4', className: 'fighter-record' });
        recordElement.innerText = fighter.name + ' ' + fighter.wins + '\n';
        fightersList.append(recordElement);
      });
  }
  
  return fightersList;
}
  