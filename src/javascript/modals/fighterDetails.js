import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';


export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'h3', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-characteristic' });
  const defenceElement = createElement({ tagName: 'span', className: 'fighter-characteristic' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-characteristic' });
  const imageAttributes = { src : source , alt : name + " image" };
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: imageAttributes });

  nameElement.innerText = name;
  attackElement.innerText = 'Attack: ' + attack + '\n';
  defenceElement.innerText = 'Defense: ' + defense + '\n';
  healthElement.innerText = 'Health: ' + health + '\n';
  
  fighterDetails.append(nameElement);
  fighterDetails.append(imageElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenceElement);
  fighterDetails.append(healthElement);
  
  return fighterDetails;
}
