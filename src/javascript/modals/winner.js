import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  const title = 'Winner';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });
}
  
function createWinnerDetails(fighter) {
  const { name, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'h3', className: 'fighter-name' });
  const imageAttributes = { src : source , alt : name + " image" };
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: imageAttributes });

  nameElement.innerText = name;
    
  fighterDetails.append(nameElement);
  fighterDetails.append(imageElement);
  
  return fighterDetails;
}
  