import { showWinnerModal } from './modals/winner';

export function fight(firstFighter, secondFighter) {
  firstFighter = Object.assign({}, firstFighter);
  secondFighter = Object.assign({}, secondFighter);
  let winner = null;

  while (winner == null) {
    let firstPunch = Math.floor(Math.random() * 2); 
    if (firstPunch === 0) {
      winner = battleRound(firstFighter, secondFighter);
    } else {
      winner = battleRound(secondFighter, firstFighter);
    }
  }
  return winner;
}

function battleRound(firstFighter, secondFighter) {
  let winner = null;
  secondFighter.health -= getDamage(firstFighter, secondFighter);
  firstFighter.health -= getDamage(secondFighter, firstFighter);
  if (secondFighter.health <= 0) {
    winner = firstFighter;
  } else if (firstFighter.health <= 0) {
    winner = secondFighter;
  }
  return winner;
}

export function getDamage(attacker, enemy) {
  const hitPower = getHitPower(attacker);
  const defencePower = getBlockPower(enemy);
  const damage = hitPower - defencePower;
  return Math.max(damage, 0); 
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const power = fighter.attack * criticalHitChance;
  return power
}

export function getBlockPower(fighter) {
  const dodgeChance  = Math.random() + 1;
  const power = fighter.defense * dodgeChance ;
  return power
}
